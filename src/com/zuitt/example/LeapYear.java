package com.zuitt.example;

import java.util.Scanner;

public class LeapYear {
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);

        System.out.println("Enter a year to check if it is a leap year:");
        int yearInput = input.nextInt();

        if(yearInput % 4 == 0 && yearInput % 100 != 0 || yearInput % 400 == 0){
            System.out.println(yearInput + " is a leap year!");
        }
        else{
            System.out.println("Sorry, "+ yearInput + " it is not a leap year.");
        }
    }
}
